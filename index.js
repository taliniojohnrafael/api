// Set-up Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors');
const app = express();


// allows access to routes defined within
	const userRoutes = require("./routes/userRoutes");
	const productRoutes = require("./routes/productRoutes");
	const addToCart = require("./routes/cartRoutes");

// MIDDLEWARES
	app.use(cors());
	app.use(express());
	app.use(express.json());
	app.use(express.urlencoded({extended:true}));

// ROUTES
	app.use("/users", userRoutes);
	app.use("/products", productRoutes);
	app.use("/users/products", addToCart);


// Database connection
	mongoose.set('strictQuery', true); // [MONGOOSE] DeprecationWarning
	
	mongoose.connect("mongodb+srv://admin:admin@zuittbatch243talinio.zbprtxo.mongodb.net/Capstone_Two?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);


	mongoose.connection.on("error", console.error.bind(console, "connection error"));
	mongoose.connection.once('open',() => console.log("Now connected to MongoDB Atlas"));

	app.listen(process.env.PORT || 4000, () =>{
		console.log(`API is now Online on port ${process.env.PORT || 4000}`);
	})



// 	// Check order if it's more than the stock available 
// module.exports.productQuantity = (request, response, next) => {
//     let userData = auth.decode(request.headers.authorization);
//     let productId = request.params.productId;

//     Product.findById(productId).then(result => { 
//         if (result.quantity < request.body.quantity){
//             return response.send(false)
//         }
//         next();
//     }).catch(error => `error here ${false}`)
// }

// module.exports.checkOutProductInsert = (request, response, next) => {
// 	let userData = auth.decode(request.headers.authorization);
// 	let productId = request.params.productId;

// 	if(userData.isAdmin){ return response.send("You don't have access to this page!")}

// 	return Product.findById(productId).then(result => {
// 		result.orders.push({
// 			orderId: userData.id
// 		})
// 		result.quantity -= request.body.products.quantity
// 		result.save();
// 		isProductUpdated = true;

// 	}).catch(error => response.send("false 1"))

// }

// module.exports.checkOutProductUserInsert = (request, response, next) => {
// 	let userData = auth.decode(request.headers.authorization);
// 	let productId = request.params.productId;

// 	return User.findById(userData.id).then(result =>{
// 		result.credit = result.credit - (prodPrice * request.body.products.quantity)
// 		result.orders.push({
// 			products: {
// 				productName: prodName,
//             	quantity: request.body.products.quantity
// 			},
// 			totalAmount: prodPrice * request.body.products.quantity
// 		})
// 		result.save();
// 		isUserUpdated = true;

// 	}).catch(error => response.send("false 2"))
// }