const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Product name is required!"]
    },

    description: {
        type: String,
        required: [true, "Products description is required!"]
    },

    quantity: {
        type: Number,
        required: [true, "Products quantity is required!"]
    },

    price : {
        type : Number,
        required: [true, "Products price is required!"]
    },

    isActive : {
        type : Boolean,
        default : true
    },

    createdOn : {
            type : Date,
            default : new Date()
        },

    orders: [{
        userId: {
        	type: String,
        	required: [true, "User id is required!"]
    	},
        itemCount:{
            type: Number,
            required: [true, "Baught product/s quantity is required!"]
        }
    }]
    
})

module.exports = mongoose.model("Product", productSchema)