const Cart = require("../models/AddToCart");
const Product = require("../models/Product");

const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if the product to be addToCart doesn't existed (user)
module.exports.checkProductExists = (request, response, next) => {
	const prodID = request.params.productId;

	if(prodID.length != 24){ 
		return response.send(`Invalid Id. Please try again`);
	}


	// try changing this with .catch().. this code may be functioning but it looked bad and messy

	return Product.findById({_id: prodID}).then(result => {
		if(!result) { return response.send(`Product doesn't exist! Please try again.`) }

		return !result.isActive ? response.send(`Product is unavailable. Please try other product.`) : next()
	})
}


// Add to cart (user)
module.exports.addToCart = (request, response) =>{
	const prodID = request.params.productId;
	let userData = auth.decode(request.headers.authorization);
	let addToCart = new Cart ({
		userId : userData.id,
		productId : prodID
	})

	return Product.findById({_id: prodID}).then(result => {

		return userData.isAdmin ? response.send("You don't have access to this page!") : addToCart.save()
		.then(product => response.send(`${result.name} is added to cart successfully!`))
			.catch(error => response.send(`Error. Product not added. Please try again.`))

	}).catch(error => response.send("Error Product Name."))
	

	// return if(condition) ? t : f
}


// Show Cart
module.exports.allCarts = (request, response) => {
	let userData = auth.decode(request.headers.authorization);


	return Cart.findOne({userId : userData.id}).then(result => {
		if(result.isActive){
			return response.send(result)
		}
		return response.send(`No product yet! Add to cart first.`)
	}).catch(error => response.send(`Error. Please try again.`))
}