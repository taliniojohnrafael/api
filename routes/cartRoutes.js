const express = require("express");
const router = express.Router();
const auth = require("../auth")

const cartController = require("../controllers/cartControllers")

// Show cart (admin)
	router.get("/showAddToCart", auth.verify, cartController.allCarts);

// Add product (admin)
	router.post("/addToCart/:productId", auth.verify, cartController.checkProductExists, cartController.addToCart);


module.exports = router;